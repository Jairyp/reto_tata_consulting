# Reto Tata Consulting

## Descripción
Crear un microservicio el cual gestione la conversion del tipo de cambio de un monto

## Objetivo
* Crear proyectos usando: 
    * https://start.spring.io/
    * maven
      ```
      mvn archetype:generate -DgroupId=com.tataconsulting -DartifactId=tataconsulting -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
      ``` 

* Como usarlo de Manera Local     
  * ubicarse en la ruta base del proyecto donde se encuentra el archivo \reto_tata_consulting\docker-compose.yml
  * Luego ejecutar el siguiente comando esto levantara  la base de datos
    ```
      docker-compose up db
      ``` 
  * Luego ejecutar el siguiente comando esto levantara  la base de datos
    ```
      docker-compose up exchangerate
      ``` 
  * validar que los contenedores esten corriendo con el siguiente comando, posterior validacion se puede consumir el microservicio en localhost:8085     

    ```
      docker ps -a
      ``` 
  
![Screenshot](docker_ps.png)

* Spring boot:
    * Arquitectura (Controller, Service, Repository, Entity y Dto)

    
## Reto 
* crear 2 endpoints para convertir un monto y actualizar:
    * POST /v1/exchangerate/1
    * GET /v1/exchangerate/convertMoney?monto=10&monedaOrigen=PEN&monedaDestino=USD


## Prerequisites
**Software**
* Java SDK 8.x or higher
* Maven 3.x or higher
* Docker 20.10.x or higher
* Docker Compose: 2.2.3 or higher
* IntelliJ IDEA
