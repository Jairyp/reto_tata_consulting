package com.tataconsulting.repository;

import com.tataconsulting.core.domain.ExchangeRate;

import java.util.List;

public interface ExchangeRateRepository {

    ExchangeRate getConvertExchangeRate(String monedaOrigen,String monedaDestino);
    void update(ExchangeRate exchangeRate) ;
}
