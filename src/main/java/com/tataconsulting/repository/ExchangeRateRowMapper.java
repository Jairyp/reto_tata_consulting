package com.tataconsulting.repository;

import com.tataconsulting.core.domain.ExchangeRate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ExchangeRateRowMapper implements RowMapper<ExchangeRate>{

    @Override
    public ExchangeRate mapRow(ResultSet rs, int i) throws SQLException {

        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setId(rs.getInt("id"));
        exchangeRate.setMoneda_origen(rs.getString("moneda_origen"));
        exchangeRate.setMoneda_destino(rs.getString("moneda_destino"));
        exchangeRate.setTipo_cambio(rs.getString("tipo_cambio"));
        return exchangeRate;
    }
}
