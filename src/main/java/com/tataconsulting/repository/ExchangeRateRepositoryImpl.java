package com.tataconsulting.repository;

import com.tataconsulting.core.domain.ExchangeRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class ExchangeRateRepositoryImpl implements ExchangeRateRepository {
    private static final Logger log = LoggerFactory.getLogger(ExchangeRateRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final ExchangeRateRowMapper exchangeRateRowMapper;

    public ExchangeRateRepositoryImpl(JdbcTemplate jdbcTemplate, ExchangeRateRowMapper exchangeRateRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.exchangeRateRowMapper = exchangeRateRowMapper;
    }

    @Override
    public ExchangeRate getConvertExchangeRate(String monedaOrigen, String monedaDestino) {
        return jdbcTemplate.queryForObject("call GetConvertExchangeRate(?,?)",
                new Object[] { monedaOrigen,monedaDestino }, exchangeRateRowMapper);
    }

    @Override
    public void update(ExchangeRate exchangeRate) {
        log.info("update {}",exchangeRate.getId());

        jdbcTemplate.update("call ExchangeRateUpdate(?,?,?,?);",new Object[] { exchangeRate.getId(),
                exchangeRate.getMoneda_origen(),
                exchangeRate.getMoneda_destino(),
                exchangeRate.getTipo_cambio()});
    }

}
