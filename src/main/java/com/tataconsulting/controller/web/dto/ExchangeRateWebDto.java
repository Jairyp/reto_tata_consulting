package com.tataconsulting.controller.web.dto;

import com.tataconsulting.core.domain.ExchangeRate;

import java.io.Serializable;


public class ExchangeRateWebDto implements Serializable {

    private ExchangeRate exchangeRate;

    public ExchangeRateWebDto() {
    }

    public ExchangeRateWebDto(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(ExchangeRate exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
