package com.tataconsulting.controller.web;

import com.tataconsulting.controller.web.dto.ExchangeRateWebDto;
import com.tataconsulting.core.domain.ExchangeRate;
import com.tataconsulting.service.ExchangeRateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@ApiOperation(value = "/v1/exchangerate", tags = "exchangerate")
@RestController
@RequestMapping("/v1/exchangerate")
public class ExchangeRateController {

    public static final String X_API_FORCE_SYC_HEADER = "X-Api-Force-Sync";

    private ExchangeRateService exchangeRateService;

    public ExchangeRateController(ExchangeRateService exchangeRateService) {
        this.exchangeRateService = exchangeRateService;
    }

    @ApiOperation(value = "exchange rate")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS",response = ExchangeRateWebDto.class),
            @ApiResponse(code = 404, message = "NOT FOUND")
    })
    @GetMapping("/convertMoney")
    public HttpEntity<ExchangeRateWebDto> getConvertExchangeRate(@RequestParam(required = false,name = "monto") String monto,
                                                  @RequestParam(required=false, name = "monedaOrigen") String monedaOrigen,
                                                  @RequestParam(required=false, name = "monedaDestino") String monedaDestino) {
        ExchangeRate exchangeRate;
        if(!monedaOrigen.isEmpty() && !monedaDestino.isEmpty()) {
            exchangeRate = exchangeRateService.getConvertExchangeRate(monedaOrigen, monedaDestino);
            exchangeRate.setMonto(monto);
            exchangeRate.setMonto_tipo_cambio(new BigDecimal(monto)
                    .multiply(new BigDecimal(exchangeRate.getTipo_cambio())));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        ExchangeRateWebDto exchangeRateWebDto = new ExchangeRateWebDto(exchangeRate);
        return new ResponseEntity<>(exchangeRateWebDto, HttpStatus.OK);
    }


    @PostMapping("/{id}")
    public HttpEntity<ExchangeRateWebDto> update(@PathVariable(name = "id") int id,
                                             @RequestBody ExchangeRateWebDto exchangeRateWebDto) {


        exchangeRateWebDto.getExchangeRate().setId(id);
        exchangeRateService.update(exchangeRateWebDto.getExchangeRate());
        return ResponseEntity.ok().build();


    }

}
