package com.tataconsulting.core.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ExchangeRate implements Serializable {

    private int id;

    private String monto;

    private BigDecimal monto_tipo_cambio;

    @ApiModelProperty(notes = "moneda origen", name = "moneda_origen",value = "PEN")
    private String moneda_origen;

    @ApiModelProperty(notes = "moneda destino", name = "moneda_destino",value = "USD")
    private String moneda_destino;

    @ApiModelProperty(notes = "tipo de cambio", name = "tipo_cambio",value = "0.20")
    private String tipo_cambio;

    public ExchangeRate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public BigDecimal getMonto_tipo_cambio() {
        return monto_tipo_cambio;
    }

    public void setMonto_tipo_cambio(BigDecimal monto_tipo_cambio) {
        this.monto_tipo_cambio = monto_tipo_cambio;
    }

    public String getMoneda_origen() {
        return moneda_origen;
    }

    public void setMoneda_origen(String moneda_origen) {
        this.moneda_origen = moneda_origen;
    }

    public String getMoneda_destino() {
        return moneda_destino;
    }

    public void setMoneda_destino(String moneda_destino) {
        this.moneda_destino = moneda_destino;
    }

    public String getTipo_cambio() {
        return tipo_cambio;
    }

    public void setTipo_cambio(String tipo_cambio) {
        this.tipo_cambio = tipo_cambio;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, true);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, true);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
