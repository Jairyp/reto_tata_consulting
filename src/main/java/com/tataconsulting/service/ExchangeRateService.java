package com.tataconsulting.service;

import com.tataconsulting.core.domain.ExchangeRate;

import java.util.List;

public interface ExchangeRateService {

    ExchangeRate getConvertExchangeRate(String monedaOrigen,String monedaDestino);
    void update(ExchangeRate exchangeRate) ;
}
