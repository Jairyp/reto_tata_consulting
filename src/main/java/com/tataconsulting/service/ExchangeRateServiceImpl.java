package com.tataconsulting.service;

import com.tataconsulting.repository.ExchangeRateRepository;
import com.tataconsulting.core.domain.ExchangeRate;
import org.springframework.stereotype.Service;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private ExchangeRateRepository exchangeRateRepository;

    public ExchangeRateServiceImpl(ExchangeRateRepository exchangeRateRepository) {
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @Override
    public ExchangeRate getConvertExchangeRate(String monedaOrigen, String monedaDestino) {
        return exchangeRateRepository.getConvertExchangeRate(monedaOrigen,monedaDestino);
    }

    @Override
    public void update(ExchangeRate exchangeRate) {
        exchangeRateRepository.update(exchangeRate);
    }
}
