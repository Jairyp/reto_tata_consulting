DELIMITER //

CREATE PROCEDURE GetConvertExchangeRate (IN pmoneda_origen varchar(30),
                                            IN pmoneda_destino varchar(30))
BEGIN
SELECT * FROM exchange_rate WHERE moneda_origen = pmoneda_origen and moneda_destino = pmoneda_destino;
END //

CREATE PROCEDURE ExchangeRateUpdate (IN p_id_exchangerate INT,
                                  IN p_moneda_origen varchar(120),
                                  IN p_moneda_destino varchar(120),
                                  IN p_tipo_cambio varchar(120))
BEGIN
update exchange_rate set moneda_origen=p_moneda_origen,moneda_destino=p_moneda_destino,tipo_cambio=p_tipo_cambio
where id = p_id_exchangerate;
END //


DELIMITER ;