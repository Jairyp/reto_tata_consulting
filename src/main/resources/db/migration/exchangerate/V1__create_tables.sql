

CREATE TABLE IF NOT EXISTS exchange_rate (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `moneda_origen` VARCHAR(30) NOT NULL,
    `moneda_destino` VARCHAR(30) NOT NULL,
    `tipo_cambio` VARCHAR(30) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY  (`id`),
    INDEX `exchange_rate_idx` (`id` ASC)
    );

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('PEN','USD','3.81');

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('USD','PEN','0.25');

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('PEN','EUR','4.32');

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('EUR','PEN','0.22');

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('PEN','CAD','3.04');

insert into exchange_rate (moneda_origen,moneda_destino,tipo_cambio)
values('CAD','PEN','0.32');
